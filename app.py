from flask import Flask, Response, request
from flask.ext.cors import CORS, cross_origin
from pyquery import PyQuery
import wikipedia
import json
import os

app = Flask(__name__)
CORS(app)

@app.route("/api/v1/query", methods=["GET"])
@cross_origin()
def query():
	lang = request.args.get('lang', 'en')
	wikipedia.set_lang(lang)

	query = request.args.get('q', '')
	page = wikipedia.page(query)

	dom = PyQuery(page.html())

	image = 'http:' + dom('table.infobox a.image img').attr('src')

	rows = dom('table.infobox').eq(0).find('tr').items()
	rows = filter(lambda row: len(list(row.items('th[scope="row"]'))) > 0, rows)
	rows = map(lambda row: { 'label': row.find('th').text(), 'value': row.find('td').text() }, rows)
	rows = filter(lambda row: row['label'] != 'Website', rows)
	rows = filter(lambda row: row['label'] != 'Official website', rows)

	data = [{
		'type': 'label',
		'value': page.title
	}, {
		'type': 'image',
		'value': image
	}, {
		'type': 'text',
		'value': page.summary
	}, {
		'type': 'table',
		'value': rows
	}, {
		'type': 'link',
		'value': page.url
	}]

	return Response(json.dumps(data), mimetype='application/json')
